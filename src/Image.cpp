
#include "Image.h"
#include <iostream>
#include <fstream>
#include <cassert>
#include <cstring>
 using namespace std;
 Image::Image()
 {
 dimx=0;
 dimy=0;
 tab_pix=nullptr;

 }
 
 Image::Image(int x,int y){

       dimx=x;
       dimy=y;
       tab_pix=new Pixel[dimx*dimy];

    }
Image::~Image(){
                  dimx=0;
                  dimy=0;
                  delete [] tab_pix;
                  }
                  
Pixel Image::get_pixel( int x,int y) const
         {  assert(x<=dimx);
            assert(y<=dimy);
             return tab_pix[y*dimx +x];
              
           }
           
                   
       Pixel& Image::get_pixel(int x,int y)
         {  assert(x<=dimx);
            assert(y<=dimy);
             return tab_pix[y*dimx +x];
         } 
         
void Image::set_pixel(int x,int y,Pixel couleur)
         {    
             assert(x<=dimx && y<=dimy);
             tab_pix[y*dimx +x]=couleur;
         }
         
         
         
         
         
         
void Image::dessinRectangle(int Xmin,int Ymin,int Xmax,int Ymax,Pixel couleur)
{
             int i,j;
             for(i=Xmin;i<=Xmax;i++){
                 for(j=Ymin;j<=Ymax;j++)
                 {
                    set_pixel(i,j,couleur);

                 }
             }  
 }       
             
             
             
             
void Image::effacer(Pixel couleur)
         {
            dessinRectangle(0,0,dimx-2,dimy-2,couleur);
         }


void Image::tes_reg()
{
Pixel p;
assert(p.r==0);
assert(p.v==0);
assert(p.b==0);
Image I(5,5);
Pixel P1(100,120,130);
I.set_pixel(3,3,P1);
Pixel p2;
p2=I.get_pixel(3,3);




}


void Image::sauver(const std::string &filename) const
{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (int y = 0; y < dimy; ++y){
        for (int x = 0; x < dimx; ++x)
        {
           const Pixel &pix = get_pixel(x++, y);
            fichier << +pix.r << " " << +pix.v << " " << +pix.b << " ";
        }
     }  
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const std::string &filename)
{
    ifstream fichier( filename.c_str() );
    assert(fichier.is_open());
    unsigned char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab_pix != nullptr)
        delete[] tab_pix;
    tab_pix=new Pixel[dimx * dimy];
    for ( int y = 0; y < dimy; ++y){
        for ( int x = 0; x < dimx; ++x)
        {
            fichier >> r >> b >> g;
            get_pixel(x, y).r = r;
            get_pixel(x, y).v = g;
            get_pixel(x, y).b = b;
        }
        
        }
        
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole()
{
    cout << dimx << " " << dimy << endl;
    for (int y = 0; y < dimy; ++y)
    {
        for (int x = 0; x < dimx; ++x)
        {
            Pixel &pix = get_pixel(x, y);
            cout << +pix.r << " " << +pix.v << " " << +pix.b << " ";
        }
        cout << endl;
    }
}









