#ifndef IMAGE_H
#define _IMAGE_H

#include "Pixel.h"
#include <iostream>
#include <string>
/**
 @class Image
 
 La class image représente une image de pixel.Elle est caraterisée par un tableau de pixel et la dimension de l'image.Cette class a plusieurs méthodes expliqués.La classe image utilise aussi la classe Pixel.
 -Le constructeur par défaut
 -Le constructeur avec paramètres
 -le destructeur de l'image.
 -la méthode get_pixel
 -la méthode set_pixel
 -la méthode dessinRecangle
 -Il y'a d'autre méthode aussi

*/
class Image
{
  private:
Pixel  * tab_pix;/**Un tableau de pixel dans lequelle on va stocker les pixels de l'image
                    */
 int dimx,dimy;/**dimension de l'image.*/
public:
   Image();/**
    *@brief 
    initialise dimx et dimy à 0 et n’alloue aucune mémoire pour le tableau de pixel
    */
 
 
   Image(int x,int y); /** @brief 
   
   Constructeur de la classe : initialise dimx et dimy avec les paramètres  (x,y) (après vérification) puis   alloue le tableau de pixel dans le tas (image noire)
   *@param x le premier parametre est un entier qui indique la premiere coordonnées du pixel
  @param y le deuxième parametre est un entier indique le deuxieme coordonnées du pixel
   
   */
   
   ~Image();/** @brief 
     Destructeur de la classe : déallocation de la mémoire du tableau de pixels
    et mise à jour des champs dimx et dimy à 0*/
 
 /** 
  * @brief cette fonction retourne un pixel après vérifier qu'il appartient bien dans le tableau de pixel
  *
  *@param x le premier parametre est un entier qui indique la premiere coordonnées du pixel
  @param y le deuxième parametre est un entier indique le deuxieme coordonnées du pixel
  @return cette fonction retourne le pixel original  de coordonnées (x,y)
  
  */
 
   Pixel get_pixel(int x,int y) const;
   /** 
  * @brief cette fonction retourne un pixel après vérifier qu'il appartient bien dans le tableau de pixel
  *
  *@param x le premier parametre est un entier indique la premiere coordonnées du pixel
  @param y le deuxième parametre est un entier qui indique le deuxieme coordonnées du pixel
  @return cette fonction retourne une copie du  pixel   de coordonnées (x,y)
  
  */
 
   Pixel& get_pixel( int x,int y);
 
     /**
     * @brief cette procédure modifie le pixel de coordonnée (x,y) en lui affectant le troisième paramètre  et vérifier sa validité
     *@param x le premier parametre est un entier qui  indique la premiere coordonnées du pixel
      @param y le deuxième parametre est un entier qui indique le deuxieme coordonnées du pixel
      @param couleur le troisième paramètre est du type pixel qui sera affecter au pixel en question
      @ret
      
     
    
     */                          
   void set_pixel(int x,int y,Pixel couleur);
   
   /** 
   * @brief cette procédure dessin un rectangle plein de couleur en faisant appelle à set_pixel pour chaque pixel du ractangle
   
   @param Xmin le premier parametre est un entier qui  indique  la première coordonnées du point inférieur gauche du ractangle
    @param Ymin le deuxième parametre est un entier qui  indique  la deuxième coordonnées du point inférieur gauche du ractangle
    
     @param Xmax le troisième parametre est un entier qui  indique  la première coordonnées du point supérieure      droite  du ractangle
     
      @param Ymax le quatrième parametre est un entier qui  indique  la deuxième  coordonnées du point supérieur droite  du ractangle
      
      @param  couleur le dernier parametre modifie tous les pixel du rectangle par une couleur
   
   
   */
   
   void dessinRectangle(int Xmin,int Ymin,int Xmax,int Ymax,Pixel couleur);
   
   /**
   * @brief cette procédure  Efface l'image en la remplissant de la couleur passée paramètre(en faisant appele à dessinrectangle
   
    @param couleur ce paramètre est du type pixel qui indique la couleur du traingle effacé
   
   */
   
   void effacer(Pixel couleur);
   
   /**
   * @brief Cette procedure Effectue une série de tests vérifiant que toutes les fonctions fonctionnent et
   font bien ce qu’elles sont censées faire, ainsi que les données membres de
   l'objet sont conformes
   
   */
   void tes_reg();
   
   /**
   @brief cette procédure sauvgarde tous les pixel qui sont dans l'image du fichier passés en paramètre
   @param filename ce paramètre est un string qui sera le nom du fichier à ouvrir.
   */
   void sauver(const std::string &filename) const;
   /**
   
   @brief cette procédure lit les pixel de l'image stocké dans le fichier qui est passées en paramètres en faisant appelle à get_pixel
   @param filename ce paramètre est un string qui sera le nom du fichier à ouvrir
   */
   void ouvrir(const std::string &filename);
   
   /**
   
   @brief cette procédure affiche sur la console les dimension de l'image et tous les pixels associés en faisant appelle à get_pixel.
   */
   
   void afficherConsole();

  
};


#endif

