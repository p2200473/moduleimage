#ifndef PIXEL_H
#define _PIXEL_H
#include <iostream>

/**
@class Pixel
La classe pixel représente un pixel avec trois composant rouge,vert et bleu.Elle a Trois méthodes
-Le constrcuteur par défaut
-Le constructeur avec les paramètres
-L'operator d'affectation.


*/

class Pixel
{

public:
 unsigned char r,v,b;//les trois composant de la pixel
 
 /**
 @brief Constructeur par défaut de la class  Initialise les champs de la pixel par 0
 */
 
  Pixel();
  
  /**
  @brief Ce constrcucteur Initialise un pixel avec trois argument passés en paramètre
  @param rr Ce parametre de type unsgigned char pour Initialiser le premier champs de la class
   @param vv Ce parametre de type unsgigned char pour Initialiser le deuxieme  champs de la class
    @param bb Ce parametre de type unsgigned char pour Initialiser le troisieme champs de la class
  *
  */
  
  Pixel (unsigned char rr,unsigned char vv,unsigned char bb);
  
  /*Pixel operator = (const Pixel& P);*/
  
  
  


};








#endif
